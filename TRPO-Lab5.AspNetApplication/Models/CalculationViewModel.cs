﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static TRPO_Lab3.Lib.CylinderArea;

namespace TRPO_Lab5.AspNetApplication.Models
{
    public class CalculationViewModel
    {
        public const double PI = 3.14;

        public double Radius { get; set;}

        public double Height { get; set; }

        public double Area { get; set; }

    }
}

