﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TRPO_Lab5.AspNetApplication.Models;
using static TRPO_Lab3.Lib.CylinderArea;

namespace TRPO_Lab5.AspNetApplication.Controllers
{
    public class CalculationController : Controller
    {
        [HttpGet]
        public ActionResult Calc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calc(string R, string H)
        {
            double area = GetCylinderArea(Convert.ToDouble(R), Convert.ToDouble(H));
            ViewBag.Result = Math.Round(area, 3) + " у. ед.";
            return View();
        }
    }
}